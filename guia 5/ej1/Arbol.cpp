#include <stdio.h>
#include <string>
#include "Arbol.h"

using namespace std;

void Arbol::insercion(NODO **nodocabeza, int *BO, string infor) {
	NODO *nodo = NULL;
	NODO *nodo1 = NULL;
	NODO *nodo2 = NULL;

	nodo = *nodocabeza;

	if (nodo != NULL) {

		if (infor < nodo->info) {
			this->insercion(&(nodo->izq), BO, infor);

			if (*BO == TRUE) {

				// FE es el factor de equilibrio de esta rama del árbol
				// sirve para determinar hacia dónde tenemos que mover los nodos al insertar otro
				switch (nodo->FE) {
					case 1:
						nodo->FE = 0;
						*BO = FALSE;
						break;

					case 0:
						nodo->FE = -1;
						break;

					case -1:
						/* reestructuración del árbol */
						nodo1 = nodo->izq;

						/* Rotacion izq-izq */
						if (nodo1->FE <= 0) {
							nodo->izq = nodo1->der;
							nodo1->der = nodo;
							nodo->FE = 0;
							nodo = nodo1;

						} else {
							/* Rotacion izq-der */
							nodo2 = nodo1->der;
							nodo->izq = nodo2->der;
							nodo2->der = nodo;
							nodo1->der = nodo2->izq;
							nodo2->izq = nodo1;

							if (nodo2->FE == -1)
								nodo->FE = 1;
							else
								nodo->FE =0;

							if (nodo2->FE == 1)
								nodo1->FE = -1;
							else
								nodo1->FE = 0;

							nodo = nodo2;
						}

						nodo->FE = 0;
						*BO = FALSE;
						break;
				}
			}

		} else {

			// si no tenemos que reorganizar el árbol entonces hay que ver dónde insertarlo
			// si la inserción es mayor a este nodo, meterlo a la derecha
			if (infor > nodo->info) {
				this->insercion(&(nodo->der), BO, infor);

				if (*BO == TRUE) {

					switch (nodo->FE) {

						case -1:
							nodo->FE = 0;
							*BO = FALSE;
							break;

						case 0:
							nodo->FE = 1;
							break;

						case 1:
							/* reestructuración del árbol */
							nodo1 = nodo->der;

							if (nodo1->FE >= 0) {
								/* Rotacion der-der */
								nodo->der = nodo1->izq;
								nodo1->izq = nodo;
								nodo->FE = 0;
								nodo = nodo1;

							} else {
								/* Rotacion der-izq */
								nodo2 = nodo1->izq;
								nodo->der = nodo2->izq;
								nodo2->izq = nodo;
								nodo1->izq = nodo2->der;
								nodo2->der = nodo1;

								if (nodo2->FE == 1)
									nodo->FE = -1;
								else
									nodo->FE = 0;

								if (nodo2->FE == -1)
									nodo1->FE = 1;
								else
									nodo1->FE = 0;

								nodo = nodo2;
							}

							nodo->FE = 0;
							BO = FALSE;
							break;
					}
				}
			} else {
				printf("El nodo ya se encuentra en el árbol\n");
			}
		}
	} else {
		nodo = new NODO;
		nodo->izq = NULL;
		nodo->der = NULL;
		nodo->info = infor;
		nodo->FE = 0;
		*BO = TRUE;
		// printf("Elemento insertado\n");
	}

	*nodocabeza = nodo;
}

/* esta función determina si un elemento existe en alguna parte del árbol */
void Arbol::busqueda(NODO *nodo, string infor) {
	if (nodo != NULL) {
		if (infor < nodo->info) {
			this->busqueda(nodo->izq, infor);
		} else {
			if (infor > nodo->info) {
				this->busqueda(nodo->der, infor);
			} else {
				printf("El nodo SI se encuentra en el árbol\n");
			}
		}
	} else {
		printf("El nodo NO se encuentra en el árbol\n");
	}
}

/* esta reestructura se preocupa sólo de la parte izquierda del árbol */
void Arbol::restructura1(NODO **nodocabeza, int *BO) {
	NODO *nodo, *nodo1, *nodo2;
	nodo = *nodocabeza;

	if (*BO == TRUE) {
		switch (nodo->FE) {
			case -1:
				nodo->FE = 0;
				break;

			case 0:
				nodo->FE = 1;
				*BO = FALSE;
				break;

		case 1:
			/* reestructuracion del árbol */
			nodo1 = nodo->der;

			if (nodo1->FE >= 0) {
				/* rotacion DD */
				nodo->der = nodo1->izq;
				nodo1->izq = nodo;

				switch (nodo1->FE) {
					case 0:
						nodo->FE = 1;
						nodo1->FE = -1;
						*BO = FALSE;
						break;
					case 1:
						nodo->FE = 0;
						nodo1->FE = 0;
						*BO = FALSE;
						break;
				}
				nodo = nodo1;
			} else {
				/* Rotacion DI */
				nodo2 = nodo1->izq;
				nodo->der = nodo2->izq;
				nodo2->izq = nodo;
				nodo1->izq = nodo2->der;
				nodo2->der = nodo1;

				if (nodo2->FE == 1)
					nodo->FE = -1;
				else
					nodo->FE = 0;

				if (nodo2->FE == -1)
					nodo1->FE = 1;
				else
					nodo1->FE = 0;

				nodo = nodo2;
				nodo2->FE = 0;
			}
			break;
		}
	}
	*nodocabeza = nodo;
}

/* y esta se preocupa de la parte derecha del árbol */
void Arbol::restructura2(NODO **nodocabeza, int *BO) {
	NODO *nodo, *nodo1, *nodo2;
	nodo = *nodocabeza;

	if (*BO == TRUE) {
		switch (nodo->FE) {
			case 1:
				nodo->FE = 0;
				break;
			case 0:
				nodo->FE = -1;
				*BO = FALSE;
				break;
			case -1:
				/* reestructuracion del árbol */
				nodo1 = nodo->izq;
				if (nodo1->FE<=0) {
					/* rotacion II */
					nodo->izq = nodo1->der;
					nodo1->der = nodo;
					switch (nodo1->FE) {
						case 0:
							nodo->FE = -1;
							nodo1->FE = 1;
							*BO = FALSE;
							break;
						case -1:
							nodo->FE = 0;
							nodo1->FE = 0;
							*BO = FALSE;
							break;
					}
					nodo = nodo1;
				} else {
					/* Rotacion ID */
					nodo2 = nodo1->der;
					nodo->izq = nodo2->der;
					nodo2->der = nodo;
					nodo1->der = nodo2->izq;
					nodo2->izq = nodo1;

					if (nodo2->FE == -1)
						nodo->FE = 1;
					else
						nodo->FE = 0;

					if (nodo2->FE == 1)
						nodo1->FE = -1;
					else
						nodo1->FE = 0;

					nodo = nodo2;
					nodo2->FE = 0;
				}
				break;
		}
	}
	*nodocabeza = nodo;
}

/* esta función quita un nodo del árbol */
void Arbol::borra(NODO **aux1, NODO **otro1, int *BO) {
	NODO *aux, *otro;
	aux = *aux1;
	otro = *otro1;

	if (aux->der != NULL) {
		this->borra(&(aux->der), &otro, BO);
		this->restructura2(&aux, BO);
	} else {
		otro->info = aux->info;
		aux = aux->izq;
		*BO = TRUE;
	}
	*aux1 = aux;
	*otro1 = otro;
}

/* intenta de buscar un número y encontrar su nodo para luego eliminarlo */
void Arbol::eliminacion(NODO **nodocabeza, int *BO, string infor) {
	NODO *nodo, *otro;

	nodo = *nodocabeza;

	if (nodo != NULL) {
		if (infor < nodo->info) {
			this->eliminacion(&(nodo->izq), BO, infor);
			this->restructura1(&nodo, BO);
		} else {
			if (infor > nodo->info) {
				this->eliminacion(&(nodo->der), BO, infor);
				this->restructura2(&nodo, BO);
			} else {
				otro = nodo;
				if (otro->der == NULL) {
					nodo = otro->izq;
					*BO = TRUE;
				} else {
					if (otro->izq==NULL) {
						nodo=otro->der;
						*BO=TRUE;
					} else {
						this->borra(&(otro->izq), &otro, BO);
						this->restructura1(&nodo, BO);
						// :)
						// free(otro);
					}
				}
			}
		}
	} else {
		printf("El nodo NO se encuentra en el árbol\n");
	}
	*nodocabeza=nodo;
}
