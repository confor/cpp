#include <iostream>
#include <fstream>
#include <string>
#include "Arbol.h"
#include "Grafo.h"

using namespace std;

int pedir_entero(string prompt) {
	cout << prompt;

	int i;
	while (!(cin >> i)) {
		cin.clear();
		cin.ignore(4000, '\n');
		cout << "Error: debe ser un número." << endl;
		return pedir_entero(prompt);
	}

	return i;
}

string pedir_string(string prompt) {
	cout << prompt;

	string i;
	while (!(cin >> i)) {
		cin.clear();
		cin.ignore(4000, '\n');
		cout << "Error: ingrese texto" << endl;
		return pedir_string(prompt);
	}

	return i;
}

/*
 * Muestra un menu de opciones.
 */
int menu() {
	int Op;

	do {
		printf("\n--------------------\n");
		printf("1) Cargar IDS de PDB\n");
		printf("2) Insertar ID\n");
		printf("3) Buscar ID\n");
		printf("4) Eliminar ID\n");
		printf("5) Grafo (png)\n");
		printf("6) Grafo (svg)\n");
		printf("0) Salir\n\n");
		Op = pedir_entero("Opción: ");
	} while (Op<0 || Op>7);

	return (Op);
}

/*
 * Función principal
 */
int main(int argc, char **argv) {
	int opcion;
	string elemento;
	NODO *raiz = NULL;
	Arbol *arbol = new Arbol;
	Grafo *grafo = new Grafo;

	fstream archivo;
	string linea;

	system("clear");
	opcion = menu();
	int inicio;

	while (opcion) {
		switch (opcion) {
			case 1:
				archivo.open("../pdb_ids.txt", ios::in);
				if (!archivo.is_open()) {
					printf("No se puede leer el archivo pdb_ids.txt\n");
					break;
				}

				printf("Cargando IDs...");

				while (getline(archivo, linea)) {
					cout << " " << linea;
					arbol->insercion(&raiz, &inicio, linea);
				}

				printf("\nArchivo cargado\n");
				break;

			case 2:
				elemento = pedir_string("Ingresar un ID: ");
				inicio = FALSE;
				arbol->insercion(&raiz, &inicio, elemento);
				// grafo->generar(raiz);
				break;

			case 3:
				elemento = pedir_string("Buscar ID: ");
				arbol->busqueda(raiz, elemento);
				break;

			case 4:
				elemento = pedir_string("Eliminar un ID: ");
				inicio = FALSE;
				arbol->eliminacion(&raiz, &inicio, elemento);
				// grafo->generar(raiz);
				break;

			case 5:
				if (raiz == NULL) {
					printf("No hay nada para graficar\n");
					break;
				}
				grafo->generar(raiz, "png");
				printf("Se creó el árbol en grafo.png en la carpeta actual\n");
				break;

			case 6:
				if (raiz == NULL) {
					printf("No hay nada para graficar\n");
					break;
				}
				grafo->generar(raiz, "svg");
				printf("Se creó el árbol en grafo.svg en la carpeta actual\n");
				break;

			case 0:
				return 0;
		}

		opcion = menu();
	}

	return 0;
}
