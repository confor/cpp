#include <string>
#ifndef GRAFO_H
#define GRAFO_H

class Grafo {
private:
	void recorrer(NODO *a, FILE *fp);
public:
	void generar(NODO *arbol, string formato);
};

#endif
