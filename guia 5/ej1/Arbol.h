#include <string>

using namespace std;

#ifndef TREE_H
#define TREE_H

#define TRUE 1
#define FALSE 0

/* estructura del nodo */
typedef struct _NODO {
  struct _NODO *izq;
  struct _NODO *der;
  string info;
  int FE;
} NODO;

class Arbol {
public:
	void insercion(NODO **nodocabeza, int *BO, string infor);
	void busqueda(NODO *nodo, string infor);
	void restructura1(NODO **nodocabeza, int *BO);
	void restructura2(NODO **nodocabeza, int *BO);
	void borra(NODO **aux1, NODO **otro1, int *BO);
	void eliminacion(NODO **nodocabeza, int *BO, string infor);
};

#endif
