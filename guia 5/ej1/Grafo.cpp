#include <iostream>
#include <string>
#include "Arbol.h"
#include "Grafo.h"

using namespace std;

/**
 * esta función toma cada nodo y escribe código para graphviz.
 * realmente esta función es la que crea el grafo, ya que aquí se une todo.
 * los nodos terminados en "d" van a ser los descendientes derechos
 * y los terminados en "i" serán los izquierdos.
 * también se dibuja el factor de equilibrio en la flecha que une dos nodos
 */
void Grafo::recorrer(NODO *a, FILE *fp) {
	char cadena[16];

	// advertencia: este código es asqueroso

	if (a != NULL) {
		if (a->izq != NULL) {
			fprintf(fp, "\"%s\" -> \"%s\" [label=%d];\n", a->info.c_str(), a->izq->info.c_str(), a->izq->FE);
		} else {
			sprintf(cadena, "%s", a->info.c_str());
			fprintf(fp, "\"%si\" [shape=point];\n", cadena);
			fprintf(fp, "\"%s\" -> \"%si\";\n", a->info.c_str(), cadena);
		}

		if (a->der != NULL) {
			fprintf(fp, "\"%s\" -> \"%s\" [label=%d];\n", a->info.c_str(), a->der->info.c_str(), a->der->FE);
		} else{
			sprintf(cadena, "%s", a->info.c_str());
			fprintf(fp, "\"%sd\" [shape=point];\n", cadena);
			fprintf(fp, "\"%s\" -> \"%sd\";\n", a->info.c_str(), cadena);
		}

		this->recorrer(a->izq, fp);
		this->recorrer(a->der, fp);
	}
}

/***
 * generar un grafo a partir del nodo raíz dado.
 * el código fuente lo guarda en grafo.txt y crea una imágen en grafo.png
 * (depende de graphviz instalado en el sistema)
 * graphviz soporta pdf, svg, png, gif, jpg, entre otros. consultar dot(1)
 */
void Grafo::generar(NODO *ArbolInt, string formato) {
	FILE *fp;

	fp = fopen("grafo.txt", "w");

	// a pesar de que aquí se arma el archivo .dot,
	// todo lo que une nodos está en la función de arriba
	fprintf(fp, "%s\n", "digraph G {");
	fprintf(fp, "%s\n", "node [style=filled fillcolor=yellow];");

	fprintf(fp, "nullraiz [shape=point];\n");
	fprintf(fp, "nullraiz -> \"%s\" [label=%d];\n", ArbolInt->info.c_str(), ArbolInt->FE);
	this->recorrer(ArbolInt, fp);

	fprintf(fp, "}\n");
	fclose(fp);

	string comando = "dot -T" + formato + " -ografo." + formato + " grafo.txt";
	system(comando.c_str());
	// system("eog grafo.png &"); // ew, gnome
}
