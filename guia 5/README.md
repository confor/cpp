## Guía 5: Árboles balanceados
#### Unidad II de Algoritmos y estructura de datos

### Guía
Dados los archivos que contienen IDs de estructuras de proteinas en PDB,

- reescriba el programa original avl.c a C++
- cree un árbol balanceado que contenga los IDs
- lea tal información desde los archivos entregados
- debe tener las operaciones:
  - insertar id
  - elimianr id
  - modificar un id buscado
  - generar un grafo del árbol (con graphviz)
- mantener los elementos ordenados
- tener menú
- usar clases

### Solución
El programa basado en `avl.c` puede leer los IDs del archivo de texto, insertar IDs de forma manual, eliminar o buscar un ID. También, usando graphviz, se puede generar un grafo del árbol entero que se exporta a un archivo `png`.

Es una transformación directa del `avl.c` entregado por el profesor, con la misma lógica del ejercicio anterior.

### Cómo usar
Un makefile facilita la compilación

    cd ej1
    make
    ./programa

El programa no toma ningún argumento, ya que presenta un menú con varias opciones al ejecutar. Tampoco existe manera de visualizar el árbol desde dentro del programa, porque los archivos dados por el profesor tienen más de 170 mil elementos.

La primera opción del menú lee `pdb_ids.txt` y agrega cada línea al árbol, manteniendo el orden. Este archivo trae 30 IDs seleccionados al azar (`shuf | head -n 30`), ya que procesar los 170 mil IDs del profesor toma mucho tiempo. Instrucciones de cómo usarlo más abajo.

En el menú existen otras opciones para agregar, buscar o eliminar un ID ingresado. Las últimas dos opciones del menú son para generar un grafo con `graphviz` que se va a exportar a `grafo.png` o `grafo.svg` en la carpeta actual, como se muestra en la figura:

![grafo de ejemplo](grafo.png)

#### Importante nota
Los siete archivos provistos por el profesor han sido concatenados en `rcsb_pdb_ids_690ad8320d15fa2f01c3406df613189b.txt`, pero por defecto no están cargados porque es una tarea considerable procesar 170 mil elementos en graphviz. Para usarlo se puede sobreescribir `pdb_ids.txt`:

	gzip -d rcsb_pdb_ids_690ad8320d15fa2f01c3406df613189b.txt.gz
    mv rcsb_pdb_ids_690ad8320d15fa2f01c3406df613189b.txt pdb_ids.txt
    cd ej1
    ./programa

Nuevamente, dibujar un grafo con 170 mil elementos requiere de tiempo, buen procesador y mucha memoria (debería ser lineal para svg, estimo 260 mb por 10000 IDs, entonces como 1300 mb para 170000 IDs).

### Requisitos
- g++
- make
- graphviz

Desarrollado en Ubuntu 20.04.


### Resultados
Evaluado con un 52.

Revisión:
1. Genera el grafo pero no lo muestra automáticamente.
2. Programa no presenta opción de modificar.
3. Sin readme.
4. Prácticamente archivos casi sin comentarios.

El punto (1) es porque se esperaba un `system("eog grafo.png")` en el código como dice el ejemplo del profesor. Esto no lo incluí porque asume que todos tienen gnome instalado, o alternativamente tendría que incluir gnome en los requisitos.

El punto (2) es porque se debería poder buscar un ID para modificarlo pero no lo hice.
