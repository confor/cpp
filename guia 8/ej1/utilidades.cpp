#include <chrono>
#include <stdlib.h>
#include "ordenamiento.h"
#include "utilidades.h"

using namespace std::chrono;

// convertir un string a un entero largo
int str2int(char const *str) {
	// strtol convierte un char[] a long int
	// la función deja lo que no pudo convertir en *resto
	char *resto;
	long l = strtol(str, &resto, 0);

	// si la conversión fue exitosa, *resto va a estar vacío
	if (!*resto)
		// implicitamente transformar long int a int
		return l;
	else
		return -1;
}

// devuelve un entero aleatorio entre min y max
int randint(int min, int max) {
	return rand() % (max - min) + min;
}

// calcula los milisegundos que le toma ordenar por selección un arreglo
int medir_seleccion(int numeros[], int cantidad) {
	Ordenamiento *ord = new Ordenamiento;

	// se anota la hora inicial,
	high_resolution_clock::time_point inicio = high_resolution_clock::now();

	// se ordena por selección
	ord->ordena_seleccion(numeros, cantidad);

	// y se anota la hora final
	high_resolution_clock::time_point final = high_resolution_clock::now();

	// a la hora final se le resta la inicial para obtener la diferencia.
	// chrono tiene templates para especificar la unidad que uno desea.
	// usé milisegundos ya que eso dice la guía
	int ms = duration_cast<milliseconds>(final - inicio).count();

	return ms;
}

// calcula los milisegundos que toma ordenar un arreglo (quicksort)
int medir_quicksort(int numeros[], int cantidad) {
	Ordenamiento *ord = new Ordenamiento;
	high_resolution_clock::time_point inicio = high_resolution_clock::now();
	ord->ordena_quicksort(numeros, cantidad);
	high_resolution_clock::time_point final = high_resolution_clock::now();
	return duration_cast<milliseconds>(final - inicio).count();
}
