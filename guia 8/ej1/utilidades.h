#ifndef UTILIDADES_H
#define UTILIDADES_H

int str2int(char const *str);
int randint(int min, int max);
int medir_seleccion(int numeros[], int cantidad);
int medir_quicksort(int numeros[], int cantidad);

#endif
