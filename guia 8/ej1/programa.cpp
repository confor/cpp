#include <iostream>
#include <string>

#include "ordenamiento.h"
#include "utilidades.h"

using namespace std;

int main(int argc, char const *argv[]) {
	// mostrar sintaxis si no se dieron dos argumentos
	// (se revisa contra 3 porque el primero es el binario de este programa)
	if (argc != 3) {
		cout << "Uso:\n\t./programa <numero> <s|n>\n";
		cout << "Ejemplo:\n\t./programa 1000 s\n";
		cout << "Donde:\n";
		cout << "\t<numero> es una cantidad entre 0 y 1000000,\n";
		cout << "\t<s|n> es si se quiere imprimir el arreglo entero (si/no)\n";
		return 1;
	}

	// transformar el primer argumento a un entero
	int cantidad = str2int(argv[1]);

	// transformar el segundo argumento a un texto
	string parametro_mostrar = argv[2];

	// decide si es que se va a imprimir el arreglo
	bool mostrar = false;

	if (cantidad <= 0 || cantidad > 100000) {
		cout << "Error: se necesita un número (0-100000) como primer argumento\n";
		return 1;
	}

	// esto se cambia sólo y sólo si el 2do argumento es "s"
	if (parametro_mostrar == "s")
		mostrar = true;

	// para comparar la velocidad de las dos funciones, es importante que ambas
	// trabajen sobre los mismos numeros y que estén desordenados de la misma
	// manera, por lo que se crean dos arreglos iguales.
	// este arreglo es el original, al que se le meten los números desordenados
	int numeros[cantidad];
	int numeros_selecc[cantidad];
	int numeros_qs[cantidad];

	// dale una seed con el tiempo actual al generador de numeros "aleatorios"
	srand(time(0));

	// rellenar arreglo
	for (int i = 0; i < cantidad; ++i) {
		numeros[i] = randint(1, 1000);
		numeros_selecc[i] = numeros[i];
		numeros_qs[i] = numeros[i];
	}

	// medir las dos funciones
	int seleccion_ms = medir_seleccion(numeros_selecc, cantidad);
	int quicksort_ms = medir_quicksort(numeros_qs, cantidad);

	// mostrarlas en una tablita
	cout << "-------------------------\n";
	cout << " Método        | Tiempo  \n";
	cout << "-------------------------\n";
	cout << " Selección     | " << seleccion_ms << " ms\n";
	cout << " Quicksort     | " << quicksort_ms << " ms\n";
	cout << "-------------------------\n";

	if (mostrar) {
		// imprimir en el formato a[0]=123 a[1]=456 a[2]=789 ... (así aparece en la guía)
		cout << "Original: ";
		for (int i = 0; i < cantidad; ++i)
			cout << "a[" << i << "]=" << numeros[i] << " ";
		cout << endl;

		cout << "Selección: ";
		for (int i = 0; i < cantidad; ++i)
			cout << "a[" << i << "]=" << numeros_selecc[i] << " ";
		cout << endl;

		cout << "Quicksort: ";
		for (int i = 0; i < cantidad; ++i)
			cout << "a[" << i << "]=" << numeros_qs[i] << " ";
		cout << endl;
	}

	return 0;
}
