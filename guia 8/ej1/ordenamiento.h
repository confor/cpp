#ifndef ORDENAMIENTO_H
#define ORDENAMIENTO_H

class Ordenamiento {
public:
	void ordena_seleccion(int a[], int n);
	void ordena_quicksort(int a[], int n);
};

#endif
