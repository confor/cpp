# Guia 3 - AED
Quinta tarea: listas enlazadas simples.

## Cómo usar
Para compilar y ejecutar:

    make
    ./programa

Ver requisitos y software usado en el [README principal](../README.md).

## Instrucciones del profesor
1. Escriba un programa que solicite números enteros y los vaya ingresando a una lista enlazada simple ordenada. Por cada ingreso debe mostrar el estado de la lista.
2. Escriba un programa que genere dos listas enlazadas ordenadas (lea los datos) y forme una tercera lista que resulte de la mezcla de los elementos de ambas listas. Muestre el contenido finalmente de las tres listas.
3. Considere que tiene una lista enlazada simple de número enteros, ordenados crecientemente (Lea los datos). Donde pueden existir valores no correlativos (ejemplo: 10 - 11 - 15 - 16 - 20). Escriba un programa que complete la lista, de tal manera que la misma, una vez modificada almacene todos los valores a partir del número del primer nodo hasta el último número del último nodo. Para el ejemplo, la lista guardará los números: 10 - 11 - 12 - 13 - 14 - 15 - 16 - 17 - 18 - 19 - 20.

## Resultado
Evaluado con un 65.

Literalmente en la revisión:

> Sin readme.

Prioridades.
