#include <iostream>
#include "Lista.h"

using namespace std;

int pedir_entero(string prompt) {
	cout << prompt;

    int i;

    while (!(cin >> i)) {
        cin.clear();
        cin.ignore(4000, '\n');
        cout << "Error: debe ser un número." << endl;
        return pedir_entero(prompt);
    }

    return i;
}

Lista *crear_lista(int total) {
	Lista *lista = new Lista();

	for (int i = 0; i < total; ++i) {
		int num = pedir_entero("> ");

		if (lista->raiz == NULL)
			lista->crear(num);
		else
			lista->agregar(num);
	}

	return lista;
}

int main() {
	cout << "listas enlazadas ordenadas" << endl
	     << "apretar enter para ingresar número" << endl;

	int total;

	total = pedir_entero("cuantos numeros tiene la primera lista? ");

	if (total < 2) {
		cout << "se necesitan mínimo dos números";
		return 1;
	}

	Lista *lista1 = crear_lista(total);

	total = pedir_entero("cuantos numeros tiene la segunda lista? ");

	if (total < 2) {
		cout << "se necesitan mínimo dos números";
		return 1;
	}

	Lista *lista2 = crear_lista(total);

	cout << "Se agregaron y ordenaron estos números:" << endl;
	cout << "Lista 1: ";
	lista1->imprimir();
	cout << endl;
	cout << "Lista 2: ";
	lista2->imprimir();
	cout << endl;

	Lista *lista3 = new Lista();

	// agregar todos los elementos de la primera lista
	Nodo *tmp;

	tmp = lista1->raiz;
	while (tmp != NULL) {
		lista3->agregar(tmp->x);
		tmp = tmp->sig;
	}

	// agregar los que no se tengan aún de la segunda lista
	tmp = lista2->raiz;
	while (tmp != NULL) {
		if (!lista3->contiene(tmp->x))
			lista3->agregar(tmp->x);

		tmp = tmp->sig;
	}


	cout << "La lista mezclada es: ";
	lista3->imprimir();
	cout << endl;

	return 0;
}
