/**************************************
 * NOTA
 * este archivo fue dado por el profesor del ramo.
 */

#include <iostream>
using namespace std;

#include "Lista.h"

Lista::Lista() {}

void Lista::crear (int x) {
    Nodo *tmp;

    /* crea un nodo . */
    tmp = new Nodo;
    /* asigna la instancia de int. */
    tmp->x = x;
    /* apunta a NULL por defecto. */
    tmp->sig = NULL;

    /* si el es primer nodo de la lista, lo deja como raíz y como último nodo. */
    if (this->raiz == NULL) {
        this->raiz = tmp;
        this->ultimo = this->raiz;
    /* de lo contrario, apunta el actual último nodo al nuevo y deja el nuevo como el último de la lista. */
    } else {
        this->ultimo->sig = tmp;
        this->ultimo = tmp;
    }
}

void Lista::imprimir () {
    /* utiliza variable temporal para recorrer la lista. */
    Nodo *tmp = this->raiz;

    /* la recorre mientras sea distinto de NULL (no hay más nodos). */
    while (tmp != NULL) {
        cout << tmp->x << ' ';
        tmp = tmp->sig;
    }
}

// añadí esto para meter un numero entremedio.
void Lista::agregar(int x) {
    // caso especial: el número dado es más chico que la raíz
    if (x < this->raiz->x) {
        Nodo *viejo = this->raiz;
        Nodo *nuevo = new Nodo;
        nuevo->x = x;

        this->raiz = nuevo;
        nuevo->sig = viejo;
        return;
    }

    // un flag para marcar si es que encontramos un lugar para el número
    bool encontrado = false;

    // recorrer la lista desde la raíz
    Nodo *tmp = this->raiz;
    while (tmp->sig != NULL) {
        // ver si es que el número que viene es más grande que el dado
        if (tmp->sig->x > x) {
            /**
             * si es así, entonces: actual <= x < siguiente
             * cosa que lo metemos entremedio:
             *    --> [ actual ] -,     .---> [ siguiente ] -->
             *                    |     |
             *                    v     |
             *                   [ nuevo ]
             */
            Nodo *viejo = tmp->sig;
            Nodo *nuevo = new Nodo;
            nuevo->x = x;
            nuevo->sig = viejo;
            tmp->sig = nuevo;

            encontrado = true;
            break;
        }

        // continuar con el siguiente
        tmp = tmp->sig;
    }

    // si no se encontró un número menor al dado, entonces tirarlo al final
    if (encontrado == false) {
        this->crear(x);
    }
}

bool Lista::contiene(int x) {
    Nodo *tmp = this->raiz;

    while (tmp != NULL) {
        if (tmp->x == x)
            return true;

        tmp = tmp->sig;
    }

    return false;
}
