#include <iostream>
#include "Lista.h"

using namespace std;

int pedir_entero(string prompt) {
	cout << prompt;

    int i;

    while (!(cin >> i)) {
        cin.clear();
        cin.ignore(4000, '\n');
        cout << "Error: debe ser un número." << endl;
        return pedir_entero(prompt);
    }

    return i;
}

int main() {
	Lista *lista = new Lista();

	cout << "lista enlazada ordenada" << endl
	     << "apretar enter para ingresar número" << endl;

	int total = pedir_entero("cuantos numeros va a ingresar? ");

	if (total < 2) {
		cout << "porfa ingresa al menos dos números";
		return 1;
	}

	for (int i = 0; i < total; ++i) {
		int num = pedir_entero("> ");

		if (lista->raiz == NULL)
			lista->crear(num);
		else
			lista->agregar(num);
	}

	cout << "Se agregaron y ordenaron estos números: ";
	lista->imprimir();
	cout << endl;

	if (lista->ultimo->x - lista->raiz->x > 100) {
		cout << "desafortunadamente, la diferencia entre el número más pequeño "
		     << "y el más grande es más de 100, asi que no se va a continuar."
		     << endl;
		return 1;
	}

	for (int i = lista->raiz->x + 1; i < lista->ultimo->x; ++i) {
		if (!lista->contiene(i))
			lista->agregar(i);
	}

	cout << "La lista ahora es: ";
	lista->imprimir();
	cout << endl;

	return 0;
}
