/**************************************
 * NOTA
 * este archivo fue entregado por el profesor. dijo que lo descargaramos
 * desde educandus, asi que lo usé.
 */

#include <iostream>
using namespace std;

#ifndef LISTA_H
#define LISTA_H

/*
 * esta línea estaba en el "main" dado por el profesor, pero lo coloqué
 * aquí porque yo creo que no tiene sentido que esté en otra parte.
 */
typedef struct _Nodo {
    int x;
    struct _Nodo *sig;
} Nodo;

class Lista {
    public:
        Nodo *raiz = NULL;
        Nodo *ultimo = NULL;

        /* constructor*/
        Lista();

        /* crea un nuevo nodo, recibe una instancia de la clase Persona. */
        void crear (int x);
        /* imprime la lista. */
        void imprimir ();

        void agregar(int x);
};
#endif
