/**
 * La primera revisión de este ramo llegó extremadamente tarde considerando que
 * el semestre comenzó hace meses, y en los resultados de esta revisión se puede
 * observar una disminución de puntos por la ausencia de comentarios.
 *
 * Debido a la extrema simpleza de los ejercicios demandados por el ramo, como
 * por ejemplo el ejercicio 1 de la guia 1:
 *     "Escriba un programa que llene un arreglo de números enteros y luego
 *      obtenga como resultado la suma del cuadrado de los números [...]",
 * creí que no sería necesario comentarlo, debido a que la función `main()` de
 * mi solución tiene un total de 25 (veinte y cinco) líneas, de las cuales sólo
 * 8 (ocho) son las que implementan la solución y el resto existen sólo para
 * pedirle al usuario que ingrese un número en la terminal.
 *
 * Como consecuencia de la demanda de "ponga comentarios" en el código, he
 * decidido entretenerme un poco y llevarlo al extremo.
 *
 * Me pregunto si es que esto va a disminuir mi nota. A este ritmo supongo que
 * lo veré por ahí a finales de noviembre.
 *
 */

/**************************************
 * COMIENZO DEL PROGRAMA
 */

/**
 * con esta línea se incluye la librería para poder interactuar con stdin y
 * y stdout, quienes me permitirán mostrar texto en la pantalla del usuario
 * y también recibir texto desde el usuario y meterlo a una variable.
 */
#include <iostream>

/**
 * esta inclusión hace referencia a otro archivo incluido junto a la entrega de
 * esta tarea, "Lista.h" en esta misma carpeta, que define lo que es la clase
 * Lista, sin necesariamente implementar el "cómo". este último estará en el
 * Lista.cpp y deberá ser compilado en un objeto por el compilador.
 * a diferencia de la inclusión anterior, esta está escrita con comillas porque
 * por convención se usan las comillas para referirse a un archivo "propio",
 * mientras que las <etiquetas> son para referirse a archivos del sistema.
 * en la mayoría de los compiladores, un "archivo.h" busca primero en la carpeta
 * local y luego en las carpetas del sistema, mientras que <archivo> busca
 * primero en las carpetas del sistema y por último en la carpeta actual.
 *
 * en particular el contenido de este "Lista.h" fue obtenido desde el archivo
 * que el profesor pidió que descargaramos. más detalles dentro del mismo.
 */
#include "Lista.h"

/**
 * los nombres de espacio son la forma que tiene este lenguaje de tener varias
 * cosas con el mismo nombre sin generar conflictos.
 * con esta línea, le decimos al compilador que vomite toda la librería estandar
 * en el contexto actual, para poder usar cosas como "cout" y "cin" sin tener
 * que especificar que pertenecen al nombre de espacio estándar.
 */
using namespace std;

/**
 * aquí se declara una función para pedir un número entero al usuario.
 * esta recibe como único parámetro un texto para mostrar al usuario, como un
 * indicador de que el programa está esperando que se haga algo.
 */
int pedir_entero(string prompt) {
	// imprimir a la pantalla el texto
    cout << prompt;

    // declarar una nueva variable "i" de tipo entero
    int i;

    // mientras exista algo que "consumir" desde stdin hacia i,
    // revisar que sea válido. si es que no lo es,
    while (!(cin >> i)) {
    	// declarar que vamos a manejar el error
        cin.clear();
    	// e ignorar el buffer de lo que sea que se ingresó
        cin.ignore(4000, '\n');
        // luego avisarle al usuario que hizo algo mal
        cout << "Error: debe ser un número." << endl;
        // y finalmente volver a pedir un entero, con el mismo parámetro.
        return pedir_entero(prompt);
    }

    // si es que todo salió bien, entonces retornar el entero dado.
    return i;
}

/**
 * la función principal del programa.
 * va a ser llamada automáticamente al ejecutar el binario.
 */
int main() {
	// se instancia una nueva Lista y se asigna a "lista"
	Lista *lista = new Lista();

	// aquí imprimimos un texto a la terminal del usuario diciendo que se espera
	// que ingrese números y que aprete enter para procesar lo que escribió
	cout << "lista enlazada ordenada" << endl;
	cout << "ingresar números y apretar enter" << endl;

	// también le avisamos que puede salir en cualquier momento apretando la
	// tecla [ctrl] acompañada de [c]
	cout << "salir con ctrl+c" << endl;

	// constantemente
	while (true) {
		// pedir un entero al usuario
		int num = pedir_entero("> ");

		if (lista->raiz == NULL) {
			// si es que es el primero entonces solo ponerlo
			lista->crear(num);
		} else {
			// de lo contrario ponerlo ordenado
			lista->agregar(num);
		}

		// cada vez que se de un número, mostrar la lista
		cout << "El estado actual de la lista: ";
		lista->imprimir();

		// retornar la línea para que el siguiente "> " quede solo
		cout << endl;
	}

	// indicar que se terminó sin ningun problema.
	// en teoría nunca se llega aquí por el `while(true)`
	return 0;
}
