#ifndef PILA_H
#define PILA_H

class Pila {
private:
	int size;
	int *stack;
	int index = 0;

public:
	Pila(int size);
	int pop();
	void push(int i);
	void print();
	bool is_empty();
	bool is_full();
};

#endif