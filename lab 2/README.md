# Lab 2 - AED
Tercera tarea: pilas y colas circulares con arreglos.

## Cómo usar
Para compilar y ejecutar:

    make
    ./programa

Ver requisitos y software usado en el [README principal](../README.md).

## Instrucciones del profesor
1. implementar una pila

## Resultado
Evaluado con un 63.

Revisión:
1. Al mostrar la pila no se muestra en formato como aparece en la guía.
2. Readme solo indica como ejecutar el programa.
3. Códigos sin comentarios.

El descuento del punto (1) es porque hay un ejemplo al final de la guía que muestra los números con dos barras verticales alrededor (`|42| |69| |100|`) pero mi programa imprime los números sin eso (`42 69 100`).

Si, descuentan puntaje por estupideces.
