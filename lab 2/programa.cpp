#include <iostream>
#include "Pila.h"

using namespace std;

void menu() {
	cout << endl << "Menú" << endl;
	cout << "1. Agregar/push" << endl;
	cout << "2. Remover/pop" << endl;
	cout << "3. Ver pila" << endl;
	cout << "0. Salir" << endl;
}

int main() {
	int n;
	cout << "Tamaño de pila: ";
	cin >> n;

	if (n <= 0) {
		cout << "Tiene que ser mayor a cero" << endl;
		return 1;
	}

	Pila pila = Pila(n);

	menu();

	int option = 0;
	while (true) {
		// menu();
		cout << endl << "Opción: ";
		cin >> option;

		if (option == 1) {
			if (pila.is_full()) {
				cout << "No se puede porque la pila está llena" << endl;
			} else {
				int x = 0;
				cout << "Ingrese número: ";
				cin >> x;
				pila.push(x);
			}
		} else if (option == 2) {
			if (pila.is_empty())
				cout << "No se puede sacar porque la pila está vacía" << endl;
			else
				cout << "Se sacó el " << pila.pop() << endl;

		} else if (option == 3) {
			if (pila.is_empty()) {
				cout << "Pila vacía";
			} else {
				cout << "Pila: ";
				pila.print();
			}
		} else if (option == 0) {
			cout << "Adiós" << endl;
			break;
		} else {
			cout << "Opción desconocida. Saliendo" << endl;
			break;
		}
	}

	return 0;
}
