#include <iostream>
#include "Pila.h"

using namespace std;

Pila::Pila(int size) {
	this->stack = new int[size];
	this->size = size;
	this->index = 0;
}

void Pila::print() {
	if (this->index == 0) {
		cout << "nada";
		return;
	}

	for (int i = 0; i < this->index; ++i)
		cout << this->stack[i] << " ";

	cout << endl;

	return;
}

int Pila::pop() {
	if (this->index == 0) {
		cout << "Pila vacía" << endl;
		return 0;
	}

	this->index--;
	int i = this->stack[this->index];
	return i;
}

void Pila::push(int i) {
	if (this->index == this->size) {
		cout << "Pila llena" << endl;
		return;
	}

	this->stack[this->index] = i;
	this->index++;
}

bool Pila::is_empty() {
	return this->index == 0;
}

bool Pila::is_full() {
	return this->index == this->size;
}
