#include <iostream>
#include "Cliente.h"

using namespace std;

bool pedir_booleano() {
	string input = "";
	cin >> input;

	if (input == "si" || input == "verdadero" || input == "true" || input == "s")
		return true;
	else
		return false;
}

int main() {
	int n = 0;
	cout << "cuantos clientes registrar? ";
	cin >> n;
	Cliente clientes[n];

	cin.ignore(); // a veces "cin >> xxxx" no funciona pero esto lo arregla

	for (int i = 0; i < n; ++i) {
		string nombre = "";
		string telefono = "";
		int saldo = 0;
		bool moroso = false;

		cout << "cliente " << (i+1) << endl;
		cout << "- nombre: ";
		getline(cin, nombre);

		cout << "- telefono: ";
		getline(cin, telefono);

		cout << "- saldo: ";
		cin >> saldo;

		cout << "- moroso? (si/no): ";
		moroso = pedir_booleano();

		cout << endl;

		clientes[i] = Cliente(nombre, telefono, saldo, moroso);
	}

	cout << "los clientes ingresados son:" << endl;
	for (int i = 0; i < n; ++i) {
		Cliente cliente = clientes[i];
		cout << "cliente " << cliente.get_nombre() << endl;
		cout << " teléfono: " << cliente.get_telefono() << endl;
		cout << " saldo: $" << cliente.get_saldo() << endl;

		if (cliente.get_moroso())
			cout << " moroso: si" << endl;
		else
			cout << " moroso: nope" << endl;

		cout << endl;
	}

	return 0;
}
