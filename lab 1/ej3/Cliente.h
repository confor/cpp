#include <string>

#ifndef CLIENTE_H
#define CLIENTE_H

using namespace std;

class Cliente {
private:
	string nombre;
	string telefono;
	int saldo = 0; // float?
	bool moroso = false;

public:
	Cliente();
	Cliente(string nombre, string telefono, int saldo, bool moroso);
	void set_nombre(string nombre);
	string get_nombre();
	void set_telefono(string telefono);
	string get_telefono();
	void set_saldo(int saldo);
	int get_saldo();
	void set_moroso(bool moroso);
	bool get_moroso();
};

#endif
