#include <iostream>
#include "Contador.h"

using namespace std;

int main() {
	int n = 0;

	cout << "cuantas frases va a ingresar? ";
	cin >> n;

	string textos[n];

	for (int i = 0; i < n; ++i) {
		string texto = "";

		cout << "ingrese frase " << (i + 1) << ": ";
		getline(cin, texto);
		textos[i] = texto;

		cout << endl;
	}

	for (int i = 0; i < n; ++i) {
		string texto = textos[i];
		Contador contador = Contador(texto);
		cout << "frase: \"" << texto << "\"" << endl;
		cout << " - " << contador.contar_mayusculas() << " mayusculas" << endl;
		cout << " - " << contador.contar_minusculas() << " minusculas" << endl;
	}

	return 0;
}
