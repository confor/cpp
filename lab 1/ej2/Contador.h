#include <string>

#ifndef CONTADOR_H
#define CONTADOR_H

class Contador {
private:
	std::string texto = "";

public:
	Contador(std::string texto);
	int contar_minusculas();
	int contar_mayusculas();
};

#endif
