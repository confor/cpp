#include <cctype>
#include <string>
#include "Contador.h"

Contador::Contador(std::string texto) {
	this->texto = texto;
}

int Contador::contar_mayusculas() {
	int total = 0;

	for (char& c : this->texto)
		if (isupper(c))
			total++;

	return total;
}

int Contador::contar_minusculas() {
	int total = 0;

	for (char& c : this->texto)
		if (islower(c))
			total++;

	return total;
}
