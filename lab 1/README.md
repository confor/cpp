# Lab 1 - AED
Segunda tarea del ramo "Algoritmos y Estructura de Datos" (2020).

## Cómo usar
Para compilar y ejecutar:

    cd ej1
    make
    ./programa

Ver requisitos y software usado en el [README principal](../README.md).

## Instrucciones del profesor
1. calcular la suma del cuadrado de los números ingresados
2. contar la cantidad de mayusculas y minúsculas que hay en un texto
3. un registro de clientes

## Resultados
Evaluado con un 55.

Revisión ejercicio 1:
1. Readme con poco contenido, solo menciona objetivo de los objetivos.
2. Faltaron comentarios en los códigos.

Revisión ejercicio 2:
1. Si quiero ingresar mas de 2 frases, el programa comienza desde la frase 2 no deja ingresar la primera frase.
2. Si ingreso 1 frase me impide el ingreso de frases.
3. Sin comentarios en los códigos.
4. Readme con poco contenido, solo menciona objetivo de los objetivos.

Revisión ejercicio 3:
1. Si se ingresa un segundo cliente no permite insertar su nombre, se salta al teléfono.
2. Readme con poco contenido, solo menciona objetivo de los objetivos.
3. Códigos sin comentarios.

No he arreglado ninguno de los problemas explicados.
