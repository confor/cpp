#include <list>

#ifndef CUADRADOS_H
#define CUADRADOS_H

class Cuadrados {
private:
	std::list<int> numeros;

public:
	void agregar(int n);
	int calcular();
};

#endif
