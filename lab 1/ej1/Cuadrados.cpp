#include "Cuadrados.h"

void Cuadrados::agregar(int n) {
	this->numeros.push_back(n);
}

int Cuadrados::calcular() {
	int total = 0;

	for (int i : this->numeros)
		total += (i * i);

	return total;
}
