#include <iostream>
#include "Cuadrados.h"

using namespace std;

int main() {
	Cuadrados cuadrados;

	int i, j, k;

	cout << "cuantos números ingresar? ";
	cin >> j;
	cout << endl;

	if (j < 1) {
		cout << "error: se necesita más de un número" << endl;
		return 1;
	}

	for (i = 0; i < j; ++i) {
		cout << "ingrese número (" << (i+1) << " de " << j << "): ";
		cin >> k;
		cout << endl;
		cuadrados.agregar(k);
	}

	cout << endl << "la suma del cuadrado de los numeros ingresados es " << cuadrados.calcular() << endl;

	return 0;
}
