# Tareas de AED
Todas mis soluciones para el ramo "Algoritmos y Estructura de Datos" (2020).

Todo está hecho en C++ y separado en varias clases, incluso cuando no es necesario. Trabajé con Ubuntu 20.04 y g++ 9.3.

Incluí mi nota en cada ejercicio (y los problemas) por si alguien quiere copiarlo.

## Cómo usar
Para copiar todas las tareas:

    git clone https://gitlab.com/confor/cpp.git

Todas las tareas usan un `makefile` y por lo general crean un binario llamado `programa` en la misma carpeta:

    make
    ./programa

### Requisitos
Es altamente probable que funcione en otros sistemas, pero esto es lo que ya tenía instalado:
- Ubuntu 20.04
- g++ 9.3
- GNU make 4.2

## Tareas
No sé por qué las tareas tienen números desordenados.

1. Guia 0 (lección 0, [link](guia 0/))
2. Lab 1 (lección 2, [link](lab 1/))
3. Lab 2 (lección 2, [link](lab 2/))
4. Guia 2 (lección 2, [link](guia 2/))
5. Guia 3 (lección 2, [link](guia 3/))
6. Guia 4 (lección 3, [link](guia 4/))
7. Guia 5 (lección 4, [link](guia 5/))
8. Guia 8 (lección 6, [link](guia 8/))

## Contenido del ramo
- Lección 0: Introducción a C++, programación orientada a objetos
- Lección 1: Análisis de algoritmos
- Lección 2: Estructuras de datos (en realidad sólo vimos listas enlazadas)
- Lección 3: Árboles binarios
- Lección 4: Árboles balanceados
- Lección 5: Grafos
- Lección 6: Métodos de ordenamiento
- Lección 7: Métodos de búsqueda
