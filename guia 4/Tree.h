#ifndef TREE_H
#define TREE_H

typedef struct Node {
	int x;
	// struct _Node *root = nullptr;
	struct Node *left = nullptr;
	struct Node *right = nullptr;
} Node;

class Tree {
public:
	Node* root = nullptr;
	Node* find(Node *node, int x);
	void remove(int x);
	Node* insert(Node *node, int x);

private:
	Node* create_node(int x);
};

#endif
