#include <iostream>
#include "Tree.h"

using namespace std;

int get_int(string prompt) {
    cout << prompt;

    int i;
    while (!(cin >> i)) {
    	cin.clear();
    	cin.ignore(4000, '\n');
        cout << "Error: debe ser un número." << endl;
        return get_int(prompt);
    }

    return i;
}

void menu() {
    cout << "----\nMenú\n----\n";
    cout << " 1. Insertar número\n";
    cout << " 2. Eliminar un número\n";
    cout << " 3. Modificar un número (lo elimina e inserta el nuevo en su lugar)\n";
    cout << " 4. Mostrar el contenido del árbol en preorden\n";
    cout << " 5. Mostrar el contenido del árbol en inorder\n";
    cout << " 6. Mostrar el contenido del árbol en posorden\n";
    cout << " 7. Exportar el árbol a una imágen png (requiere graphviz)\n";
    cout << " 8. Salir\n";
}

void print_inorder(Node *root) {
	if (root) {
		print_inorder(root->left);
		cout << " " << root->x << ",";
		print_inorder(root->right);
	}
}

void print_postorder(Node *root) {
	if (root) {
		print_postorder(root->left);
		print_postorder(root->right);
		cout << " " << root->x << ",";
	}
}

void print_preorder(Node *root) {
	if (root) {
		cout << " " << root->x << ",";
		print_preorder(root->left);
		print_preorder(root->right);
	}
}

int main() {
	bool tree_ready = false;
	Tree* tree = new Tree;
	Node* root = nullptr;

	menu();

	while (true) {
		int choice = get_int("Opción: ");

		if (choice == 1) {
			int x = get_int("Ingrese un entero: ");
			if (tree_ready)
				tree->insert(root, x);
			else {
				root = tree->insert(nullptr, 120);
				tree_ready = true;
			}

			cout << "ok" << endl;
		}

		else if (choice == 2) {
			int x = get_int("Ingrese un entero: ");
			Node *node = tree->find(root, x);

			if (!node) {
				cout << "No se encontró ese número" << endl;
				continue;
			}

			cout << "Se encontró el nodo pero el programa no implementa eliminar" << endl;
		}

		else if (choice == 3) {
			int x = get_int("Ingrese un entero: ");
			Node *node = tree->find(root, x);

			if (!node) {
				cout << "No se encontró ese número" << endl;
				continue;
			}

			cout << "Se encontró el nodo pero el programa no implementa modificar" << endl;
		}

		else if (choice == 4) {
			if (!root) {
				cout << "El árbol está vacío" << endl;
				continue;
			}

			cout << "preorder:" << endl;
			print_preorder(root);
			cout << endl;
		}

		else if (choice == 5) {
			if (!root) {
				cout << "El árbol está vacío" << endl;
				continue;
			}

			cout << "inorder:" << endl;
			print_inorder(root);
			cout << endl;
		}

		else if (choice == 6) {
			if (!root) {
				cout << "El árbol está vacío" << endl;
				continue;
			}

			cout << "postorder:" << endl;
			print_postorder(root);
			cout << endl;
		}

		else if (choice == 7) {
			if (!root) {
				cout << "El árbol está vacío" << endl;
				continue;
			}

			cout << "ah que harta fe hay en el programa xd" << endl;
		}

		else if (choice == 8) {
			cout << "Adiós" << endl;
			break;
		}

		else {
			cout << "Opción desconocida, saliendo" << endl;
			break;
		}
	}

	tree->insert(root, 87);
	tree->insert(root, 140);
	tree->insert(root, 43);
	tree->insert(root, 99);
	tree->insert(root, 130);
	tree->insert(root, 22);
	tree->insert(root, 65);
	tree->insert(root, 93);
	tree->insert(root, 135);
	tree->insert(root, 56);

	cout << "find node 43:" << endl;
	Node *test = tree->find(root, 43);
	cout << "  " << test->x << endl;

	if (test->left)
		cout << test->left->x;

	if (test->right)
		cout << "    " << test->right->x;

	cout << endl;

	return 0;
}
