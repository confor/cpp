# Guía 4 - AED
Sexta tarea: árboles binarios de búsqueda

## Sobre el programa
Este programa crea un árbol binario con números enteros dados por el usuario. Sólo permite insertar números (int) e imprimir el árbol (en preorden, inorden y posorden).

La pauta pide modificar un valor y también eliminar nodos (reorganizando el árbol), pero este programa no hace eso porque nunca lo programé por flojo y se acabó el tiempo para la entrega.

También se pide exportar el árbol binario a un gráfico usando graphviz, pero tampoco lo hice.

## Cómo usar
Es suficiente con escribir `make` en la terminal y se compilará el binario `main` en la carpeta actual:

   make
   ./main

Ver requisitos y software usado en el [README principal](../README.md).

## Notas
- Está todo en inglés
- Tiene un menú porque la pauta lo pide.
- El programa *no* implementa todas las cosas de la pauta porque soy flojo.

## Resultado
Evaluado con un 44.

Revisión y problemas:
1. Al exportar el árbol a una imagen png no se crea y por lo tanto tampoco se muestra.
2. Aparentemente no permite el ingreso del mismo numero pero no hay mensaje alguno que me lo corrobore a menos de que ingrese a los tipos de orden.
3. El programa no elimina.
4. El programa no deja modificar.
5. Prácticamente el readme no me dice nada, mas que lo que debió haber hecho.
6. Códigos sin comentar.

Curioso que exista el punto (5) dado que en la entrega original no existía un readme.

La revisión indica que faltan todas las cosas que no hice; la nota es rara porque no hice casi nada.
