#include <iostream>
#include <cstddef>
#include "Tree.h"

Node* Tree::find(Node *node, int x) {
	if (!node)
		return nullptr;

	if (x == node->x)
		return node;
	else if (x < node->x)
		return this->find(node->left, x);
	else if (x > node->x)
		return this->find(node->right, x);

	return nullptr;
}

Node* Tree::create_node(int x) {
	Node *node = new Node;
	node->x = x;
	return node;
}

void Tree::remove(int x) {
	Node *node = this->find(nullptr, x);
}

Node* Tree::insert(Node *node, int x) {
	if (!node)
		return this->create_node(x);
	else if (x < node->x)
		node->left = this->insert(node->left, x);
	else if (x > node->x)
		node->right = this->insert(node->right, x);

	return node;
}
