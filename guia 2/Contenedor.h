#include <string>

#ifndef CONTENEDOR_H
#define CONTENEDOR_H

class Contenedor {
private:
	std::string nombre = "";

public:
	Contenedor();
	void set_nombre(std::string nombre);
	std::string get_nombre();
};

#endif
