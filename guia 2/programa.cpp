#include <iostream>
#include <string>
#include <vector>
#include "Contenedor.h"
#include "Pila.h"

using namespace std;

// función para pedir un número entero a stdin
// acepta sólo y sólo enteros
int pedir_entero(string prompt) {
    cout << prompt;
    int i;

    while (!(cin >> i)) {
        cin.clear();
        cin.ignore(4000, '\n');
        cout << "Error: debe ser un número." << endl;
        return pedir_entero(prompt);
    }

    return i;
}

// muestra un dibujo de la vista general de todos los contenedores
void imprimir(vector<Pila> pilas, int alto, int ancho) {
    for (int i = alto - 1; i >= 0; --i) {
        // cout << " " << i + 1;
        for (Pila pila : pilas) {
            cout << "\t";
            if (pila.get_index() > i) {
                Contenedor cont = pila.get_stack()[i];
                cout << cont.get_nombre();
            }
        }
        cout << endl;
    }

    for (int i = 0; i < ancho; ++i)
        cout << "\t   " << i + 1;

    cout << endl;
}

int menu() {
    cout << "----\nMenú\n----\n";
    cout << " 1. Poner contenedor\n";
    cout << " 2. Mover contenedor\n";
    cout << " 3. Eliminar contenedor\n";
    cout << " 4. Ver\n";
    cout << " 5. Salir\n";

    return pedir_entero("Opción: ");
}

// le da un color "aleatorio" a un texto dado
string color(string str) {
    string colors[] = {"\033[1;31m", "\033[1;32m", "\033[1;33m", "\033[1;34m", "\033[1;35m", "\033[1;36m"};
    return colors[rand() % 6] + str + "\033[0m";
}

int main() {
    srand(123); // para los colores

    Contenedor cont1 = Contenedor();
    cont1.set_nombre(color(" uno "));
    Contenedor cont2 = Contenedor();
    cont2.set_nombre(color(" dos "));
    Contenedor cont3 = Contenedor();
    cont3.set_nombre(color("tres "));

    int alto = pedir_entero("hasta cuantos contenedores va a apilar? ");
    int ancho = pedir_entero("cuantas pilas de contenedores va a tener? ");

    if (alto <= 1 || ancho <= 2) {
        cout << "error: no tiene sentido ese tamaño, demasiado pequeño" << endl;
        return 1;
    }

    vector<Pila> pilas;

    for (int i = 0; i < ancho; ++i)
        pilas.push_back(Pila(alto));

    pilas[0].push(cont1);
    pilas[0].push(cont2);
    pilas[1].push(cont3);

    cout << endl << "Se incluyen algunos contenedores de ejemplo:" << endl;
    imprimir(pilas, alto, ancho);

    int opcion = 0;

    while (true) {
        opcion = menu();

        if (opcion == 1) {
            int pos = pedir_entero("en qué pila quiere poner el contenedor? ");
            if (0 >= pos || pos > ancho) {
                cout << "esa pila no existe. tiene que ser entre 1 y " << ancho << "." << endl;
                continue;
            }

            pos -= 1; // hay que sacarle uno para calzar con el indice del arreglo
            if (pilas[pos].is_full()) {
                cout << "esa está llena, no le cabe otro contenedor" << endl;
                continue;
            }

            Contenedor cont = Contenedor();
            cont.set_nombre(color("conte"));
            pilas[pos].push(cont);
            imprimir(pilas, alto, ancho);

        } else if (opcion == 2) {
            int origen = pedir_entero("de que pila va a sacar un contenedor? ");
            if (0 >= origen || origen > ancho) {
                cout << "esa pila no existe. tiene que ser entre 1 y " << ancho << "." << endl;
                continue;
            }

            origen -= 1;
            if (pilas[origen].is_empty()) {
                cout << "no hay nada ahi para sacar" << endl;
                continue;
            }

            int destino = pedir_entero("en que pila va a depositar el contenedor? ");
            if (0 >= destino || destino > ancho) {
                cout << "esa pila no existe. tiene que ser entre 1 y " << ancho << "." << endl;
                continue;
            }

            destino -= 1;
            if (pilas[destino].is_full()) {
                cout << "no se puede poner otro contenedor en esa pila" << endl;
                continue;
            }

            pilas[destino].push(pilas[origen].pop());
            imprimir(pilas, alto, ancho);

        } else if (opcion == 3) {
            int pos = pedir_entero("de que pila se va a eliminar un contenedor? ");
            if (0 >= pos || pos > ancho) {
                cout << "esa pila no existe. tiene que ser entre 1 y " << ancho << "." << endl;
                continue;
            }

            pos -= 1;
            if (pilas[pos].is_empty()) {
                cout << "esa pila ya está vacia" << endl;
                continue;
            }

            pilas[pos].pop();
            imprimir(pilas, alto, ancho);

        } else if (opcion == 4) {
            cout << endl << "La cosa actual tiene estos contenedores apilados:" << endl;
            imprimir(pilas, alto, ancho);

        } else if (opcion == 5) {
            cout << "adiós" << endl;
            break;

        } else {
            cout << "ah?" << endl;
        }

    }

    return 0;
}
