#include "Contenedor.h"

#ifndef PILA_H
#define PILA_H

class Pila {
private:
	int size;
	Contenedor *stack;
	int index = 0;

public:
	Pila(int size);
	Contenedor pop();
	void push(Contenedor i);
	void print();
	bool is_empty();
	bool is_full();
	Contenedor* get_stack();
	int get_size();
	int get_index();
};

#endif
