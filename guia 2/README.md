# Guia 2 - AED
Cuarta tarea: pilas con arreglos.

## Cómo usar
Al inicio se pregunta el tamaño del puerto seco. Hay un menú básico que ofrece ingresar, mover o eliminar contenedores. Cada opción preguntará en qué pila se pondrá el contenedor.

Para compilar y ejecutar:

    make
    ./programa

Ver requisitos y software usado en el [README principal](../README.md).

## Instrucciones
### Resumen
La guía pide crear un arreglo de pilas, imaginando que es un puerto con varios contenedores. Se tienen hasta M pilas y cada pila lleva hasta N contenedores.

La idea es que sólo se puede tomar un contenedor si es que no tiene ningún otro contenedor encima.

### Instrucciones del profesor
En un puerto seco se guarda mercadería en contenedores. No es posible colocar más de `n` contenedores uno encima del otro y no hay área para más de `m` pilas de contenedores. Cada contenedor tiene un número y un nombre de la empresa propietaria. `n` y `m` pueden ser distintos.

Escriba un programa que permita gestionar el ingreso y salida de contenedores. Note que para retirar un contenedor es necesario retirar los contenedores que están encima de él y colocarlos en otra pila.

## Resultado
Evaluado con un 53.

Revisión:
1. Al ingresar el contenedor los guarda con el mismo nombre.
2. Al eliminar no realiza proceso explicado en la guía, elimina como una pila normal sin redistribución en el puerto.
3. Sin readme.
4. Faltaron comentarios en los códigos.

Se que tengo malo todo el proceso de "sacar" un contenedor, porque el programa debería "encontrar" el contenedor que el usuario pide y luego automáticamente sacar todo lo que está encima. Debido a que no se pide un nombre para cada contenedor, es imposible "encontrarlo" porque son todos iguales.
