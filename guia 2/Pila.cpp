#include <iostream>
#include "Pila.h"

using namespace std;

Pila::Pila(int size) {
	this->stack = new Contenedor[size];
	this->size = size;
	this->index = 0;
}

void Pila::print() {
	if (this->index == 0) {
		cout << "nada";
		return;
	}

	for (int i = 0; i < this->index; ++i)
		cout << this->stack[i].get_nombre() << " ";

	cout << endl;

	return;
}

Contenedor Pila::pop() {
	if (this->index == 0) {
		cout << "Pila vacía" << endl;
		return Contenedor();
	}

	this->index--;
	Contenedor i = this->stack[this->index];
	return i;
}

void Pila::push(Contenedor i) {
	if (this->index == this->size) {
		cout << "Pila llena" << endl;
		return;
	}

	this->stack[this->index] = i;
	this->index++;
}

bool Pila::is_empty() {
	return this->index == 0;
}

bool Pila::is_full() {
	return this->index == this->size;
}

Contenedor* Pila::get_stack() {
	return this->stack;
}

int Pila::get_size() {
	return this->size;
}

int Pila::get_index() {
	return this->index;
}
