#include <string>
#include "Contenedor.h"

Contenedor::Contenedor() {
	this->nombre = "cont";
}

std::string Contenedor::get_nombre() {
	return "[" + this->nombre + "]";
}

void Contenedor::set_nombre(std::string nombre) {
	this->nombre = nombre;
}
