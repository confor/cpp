# Guia 0 - AED
Primera tarea: introducción a C++

## Sobre este programa
El primer ejercicio del ramo busca familiarizar con C++ y las clases. El programa mantiene varias proteínas que a su vez están hechas de cadenas, y estas de aminoácidos compuestos de átomos y cada átomo tiene diferentes coordenadas. Todo está implementado con clases y hay un menú que guía al usuario.

El programa puede ingresar una proteína (y por ende cada una de sus partes), insertar un ejemplo o mostrar un listado de las proteínas. Es sólo un ejercicio y nada se guarda.

#### Cómo usar
Para compilar y ejecutar:

    cd ej1
    make
    ./programa

Ver requisitos y software usado en el [README principal](../README.md).

#### Captura de pantalla
Inicio del programa:
```
$ ./programa
Algoritmos y estructuras de datos: Guia 0

Menú
 1. Mostrar proteinas
 2. Ingresar una proteina
 3. Añadir una proteina de ejemplo
 4. Cerrar el programa
Ingrese opción:
```

Opción "1. Mostrar proteínas":
```
Proteina: Ejemplo (ID algun_id)
 - Cadena A
    - Aminoacido 123 aminoacido1
       - Atomo 1 carbono (coords -0.22 0.40 1.90)
       - Atomo 2 carbono (coords 0.00 0.00 0.00)
    - Aminoacido 456 aminoacido2
       - Atomo 3 carbono (coords 0.00 0.00 0.00)
 - Cadena B
    - Aminoacido 789 aminoacido3
       - Atomo 4 carbono (coords 0.00 0.00 0.00)
       - Atomo 5 carbono (coords 0.00 0.00 0.00)
       - Atomo 6 carbono (coords 0.00 0.00 0.00)
```

## Instrucciones del profesor
Dado el diagrama de clases adjunto, escriba un prorama en C++ que implemente dichas clases. El programa debe permitir el ingreso de una lista de proteínas con su información asociada.

Considere además:
- por cada clase escribir su archivo .cpp y .h
- utilizar Makefile para la compilación
- implemente un menú básico para indicar las acciones

Cree un proyecto en algún repositorio Git y en la entrega indique el link correspondiente.

El uso de clases es obligatorio para la revisión y calificación.

![diagrama](diagrama.png)

## Resultados
Evaluado con un 59.

La revisión dice:
1. Al imprimir, nos muestra parte de los datos de la ultima proteína ingresada al inicio y después al final se expresa de buena manera, pero bien.
2. Archivo no presenta readme.
3. Códigos sin comentarios.

Supongo que está todo el código bueno, aunque no entiendo el punto (1).
