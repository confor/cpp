#include <list>
#include <string>
#include "Aminoacido.h"
using namespace std;

#ifndef CADENA_H
#define CADENA_H

class Cadena {
private:
	string letra = "\0";
	list<Aminoacido> aminoacidos;

public:
	Cadena(string letra);

	void set_letra(string letra);
	void add_aminoacido(Aminoacido aminoacido);
	string get_letra();
	list<Aminoacido> get_aminoacidos();
};

#endif
