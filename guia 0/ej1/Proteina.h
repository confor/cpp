#include <list>
#include <string>
#include "Cadena.h"
using namespace std;

#ifndef PROTEINA_H
#define PROTEINA_H

class Proteina {
private:
	string nombre = "\0";
	string id = "\0";
	list<Cadena> cadenas;

public:
	Proteina(string nombre, string id);

	void set_nombre(string nombre);
	void set_id(string id);
	void add_cadena(Cadena cadena);
	string get_id();
	string get_nombre();
	list<Cadena> get_cadenas();
};

#endif
