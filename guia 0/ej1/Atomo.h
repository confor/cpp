#include <string>
#include "Coordenada.h"
using namespace std;

#ifndef ATOMO_H
#define ATOMO_H

class Atomo {
private:
	string nombre = "\0";
	int numero = 0;
	Coordenada coordenada;

public:
	Atomo(string nombre, int numero);
	void set_nombre(string nombre);
	void set_numero(int numero);
	void set_coordenada(Coordenada coordenada);
	string get_nombre();
	int get_numero();
	Coordenada get_coordenada();
};

#endif
