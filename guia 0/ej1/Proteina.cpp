#include "Proteina.h"

Proteina::Proteina(string id, string nombre) {
	this->id = id;
	this->nombre = nombre;
}

void Proteina::set_nombre(string nombre) {
	this->nombre = nombre;
}

void Proteina::set_id(string id) {
	this->id = id;
}

void Proteina::add_cadena(Cadena cadena) {
	this->cadenas.push_back(cadena);
}

string Proteina::get_id() {
	return this->id;
}

string Proteina::get_nombre() {
	return this->nombre;
}

list<Cadena> Proteina::get_cadenas() {
	return this->cadenas;
}
