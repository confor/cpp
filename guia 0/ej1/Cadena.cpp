#include "Cadena.h"

Cadena::Cadena(string letra) {
	this->letra = letra;
}

void Cadena::set_letra(string letra) {
	this->letra = letra;
}

void Cadena::add_aminoacido(Aminoacido aminoacido) {
	this->aminoacidos.push_back(aminoacido);
}

string Cadena::get_letra() {
	return this->letra;
}

list<Aminoacido> Cadena::get_aminoacidos() {
	return this->aminoacidos;
}
