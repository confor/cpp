#include <iostream>
#include <list>
#include <string>
#include <stdio.h>
#include "Aminoacido.h"
#include "Atomo.h"
#include "Cadena.h"
#include "Coordenada.h"
#include "Proteina.h"

using namespace std;

void imprimir_datos_proteina(Proteina &prot) {
	cout << "Proteina: " << prot.get_nombre() << " (ID " << prot.get_id() << ")" << endl;;

	for (Cadena &cadena : prot.get_cadenas()) {
		cout << " - Cadena " << cadena.get_letra() << endl;;

		for (Aminoacido &aa : cadena.get_aminoacidos()) {
			cout << "    - Aminoacido " << aa.get_numero() << " " << aa.get_nombre() << endl;

			for (Atomo &atomo : aa.get_atomos()) {
				Coordenada coord = atomo.get_coordenada();
				cout << "       - Atomo " << atomo.get_numero() << " " << atomo.get_nombre();
				// siempre su wen sprintf pa dejar bien bellacos los decimales
				printf(" (coords %.2f %.2f %.2f)\n", coord.get_x(), coord.get_y(), coord.get_z());
				// cout << " (" << coord.get_x() << ", " << coord.get_y() << ", " << coord.get_z() << ")" << endl;
			}
		}
	}

	cout << endl;

	return;
}

// la guia pide esta función ¯\_(ツ)_/¯
void imprimir_datos_proteinas(list<Proteina> proteinas) {
	for (Proteina &prot : proteinas)
		imprimir_datos_proteina(prot);
}

Proteina leer_datos_proteina() {
	string prot_id = "prot";
	string prot_nombre;
	string cad_letra;
	string aa_nombre;
	string atomo_nombre;

	// contador
	int i_cadenas = 0;
	int i_aa = 0;
	int i_atomo = 0;

	// cantidad deseada (stdin)
	int n_cadenas;
	int n_aa;
	int n_atomos;
	float coord_x = 0.0;
	float coord_y = 0.0;
	float coord_z = 0.0;

	cout << "Ingrese nombre de proteina: ";
	cin >> prot_nombre;

	cout << "Cantidad de cadenas: ";
	cin >> n_cadenas;

	Proteina prot = Proteina(prot_id, prot_nombre);

	for (int i = 0; i < n_cadenas; i++) {
		i_cadenas++;
		cout << "Letra de la cadena " << i_cadenas << ": ";
		cin >> cad_letra;

		Cadena cadena = Cadena(cad_letra);

		cout << "Cantidad de aminoacidos para esta cadena: ";
		cin >> n_aa;

		for (int j = 0; j < n_aa; j++) {
			i_aa++;
			cout << "Nombre del aminoácido " << i_aa << ": ";
			cin >> aa_nombre;

			Aminoacido aa = Aminoacido(aa_nombre, i_aa);

			cout << "Cantidad de atomos para este aminoácido: ";
			cin >> n_atomos;

			for (int k = 0; k < n_atomos; k++) {
				i_atomo++;
				cout << "Nombre para el átomo " << i_atomo << ": ";
				cin >> atomo_nombre;

				Atomo atomo = Atomo(atomo_nombre, i_atomo);;

				cout << "Coordenada X para este átomo: ";
				cin >> coord_x;
				cout << "Coordenada Y para este átomo: ";
				cin >> coord_y;
				cout << "Coordenada Z para este átomo: ";
				cin >> coord_z;

				Coordenada coord = Coordenada(coord_x, coord_y, coord_z);
				atomo.set_coordenada(coord);
				aa.add_atomo(atomo);
			}

			cadena.add_aminoacido(aa);
		}

		prot.add_cadena(cadena);
	}

	return prot;
}

Proteina proteina_de_ejemplo() {
	Proteina prot = Proteina("algun_id", "Ejemplo");
	Coordenada coord_inicial = Coordenada(0.0, 0.0, 0.00);
	Coordenada coord1 = Coordenada(-0.22, 0.4, 1.9);

	Atomo atom1 = Atomo("carbono", 1);
	Atomo atom2 = Atomo("carbono", 2);
	Atomo atom3 = Atomo("carbono", 3);
	Atomo atom4 = Atomo("carbono", 4);
	Atomo atom5 = Atomo("carbono", 5);
	Atomo atom6 = Atomo("carbono", 6);

	atom1.set_coordenada(coord1);
	atom2.set_coordenada(coord_inicial);
	atom3.set_coordenada(coord_inicial);
	atom4.set_coordenada(coord_inicial);
	atom5.set_coordenada(coord_inicial);
	atom6.set_coordenada(coord_inicial);

	Aminoacido aa1 = Aminoacido("aminoacido1", 123);
	Aminoacido aa2 = Aminoacido("aminoacido2", 456);
	Aminoacido aa3 = Aminoacido("aminoacido3", 789);

	aa1.add_atomo(atom1);
	aa1.add_atomo(atom2);
	aa2.add_atomo(atom3);
	aa3.add_atomo(atom4);
	aa3.add_atomo(atom5);
	aa3.add_atomo(atom6);

	Cadena cadA = Cadena("A");
	Cadena cadB = Cadena("B");
	Cadena cadC = Cadena("C");

	cadA.add_aminoacido(aa1);
	cadA.add_aminoacido(aa2);
	cadB.add_aminoacido(aa3);

	prot.add_cadena(cadA);
	prot.add_cadena(cadB);

	return prot;
}

int main() {
	cout << "Algoritmos y estructuras de datos: Guia 0" << endl;
	list<Proteina> proteinas;

	while (true) {
		cout << endl << "Menú" << endl;
		cout << " 1. Mostrar proteinas" << endl;
		cout << " 2. Ingresar una proteina" << endl;
		cout << " 3. Añadir una proteina de ejemplo" << endl;
		cout << " 4. Cerrar el programa" << endl;
		cout << "Ingrese opción: ";
		int opcion;
		cin >> opcion;

		printf("\e[1;1H\e[2J"); // limpiar pantalla

		if (opcion == 1) {
			if (proteinas.size() == 0) {
				cout << "No hay ninguna proteína registrada" << endl;
				continue;
			}

			imprimir_datos_proteinas(proteinas);
		} else if (opcion == 2) {
			Proteina prot = leer_datos_proteina();
			proteinas.push_back(prot);

			printf("\e[1;1H\e[2J");
			imprimir_datos_proteina(prot);
			cout << endl << "OK" << endl;
		} else if (opcion == 3) {
			Proteina prot_ej = proteina_de_ejemplo();
			proteinas.push_back(prot_ej);
			imprimir_datos_proteina(prot_ej);
		} else {
			cout << "Adiós";
			break;
		}
	}

	cout << endl;

	return 0;
}
