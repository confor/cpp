#include <iostream>
using namespace std;

#ifndef HASH_H
#define HASH_H

class Hash {
public:
	int hash(int clave, int largo);

	void ingresoLineal(int *arreglo, int largo, int clave, int posicion);
	void ingresoCuadratica(int *arreglo, int largo, int clave, int posicion);
	void ingresoDobleHash(int *arreglo, int largo, int clave, int posicion);
	// void ingresoEncadenamiento();

	void searchLineal(int *arreglo, int largo, int clave, int posicion);
	void searchCuadratica(int *arreglo, int largo, int clave, int posicion);
	void searchDobleHash(int *arreglo, int largo, int clave, int posicion);
	// void searchEncadenamiento();
};

#endif
