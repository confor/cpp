#include <iostream>
#include "Hash.h"

using namespace std;

int get_int(string prompt) {
	cout << prompt;

	int i;
	while (!(cin >> i)) {
		cin.clear();
		cin.ignore(4000, '\n');
		cout << "Error: debe ser un número." << endl;
		return get_int(prompt);
	}

	return i;
}

void print(int arr[], int len) {
	cout << "[";

	for (int i = 0; i < len; ++i) {
		if (arr[i] == '\0')
			cout << "  _  ";
		else
			cout << "  " << arr[i] << "  ";
	}

	cout << "]";
}

bool empty(int arr[], int len) {
	while (len--)
		if (arr[len] != '\0')
			return false;

	return true;
}

int menu() {
	cout << "Menú\n";
	cout << " 1. Ingresar número\n";
	cout << " 2. Buscar número\n";
	cout << " 0. Salir\n";

	return get_int("Ingrese una opción: ");
}

int main(int argc, char *argv[]) {
	// asegurarse de que se entrega el método, o si no salir
	if (argc != 2) {
		cout << "Uso:\n\t./main metodo\n";
		cout << "Método: [L]ineal, [C]uadrática, [D]oble dirección, [E]ncadenamiento\n";
		cout << "Ejemplo: ./main L\n";
		exit(1);
	}

	string method = argv[1];

	// sólo aceptar estos métodos, de lo contrario salir
	if (method == "L")
		cout << "Método: reasignación prueba lineal\n";
	else if (method == "C")
		cout << "Método: reasignación prueba cuadrática\n";
	else if (method == "D")
		cout << "Método: reasignación doble dirección hash\n";
	else if (method == "E")
		cout << "Método: encadenamiento\n";
	else {
		cout << "Error: método desconocido. Probar con L/C/D/E\n";
		exit(1);
	}

	// !!!!!!!!!!!
	if (method == "E") {
		cout << "ERROR: este método no está implementado!!!!\n";
		exit(1);
	}

	// inicializar arreglo con el tamaño dado
	int len = get_int("¿Largo del arreglo? ");
	int arr[len];

	// llenar arreglo de nulo
	for (int i = 0; i < len; ++i)
		arr[i] = '\0';

	Hash *hash = new Hash();
	int i;
	int pos;

	while (true) {
		cout << "\nArreglo actual:\n ";
		print(arr, len);
		cout << "\n";

		switch (menu()) {
		case 1:
			// ingresar un número
			i = get_int("Ingrese un número: ");
			pos = hash->hash(i, len);

			// si no hay nada en la posición calculada, insertarlo. de lo contrario,
			// hay que calcular una nueva posición según el método seleccionado
			if (arr[pos] == '\0') {
				arr[pos] = i;
				cout << "Insertado en " << pos << "\n";
			} else {
				cout << "Colisión al insertar en posición " << pos << ", recalculando\n";

				if (method == "L")
					hash->ingresoLineal(arr, len, i, pos);
				else if (method == "C")
					hash->ingresoCuadratica(arr, len, i, pos);
				else if (method == "D")
					hash->ingresoDobleHash(arr, len, i, pos);
				// else if (method == "E")
					// hash->ingresoEncadenamiento();
			}

			break;

		case 2:
			if (empty(arr, len)) {
				cout << "No se puede buscar porque está vacío\n";
				break;
			}

			// buscar la posición de un número
			i = get_int("Ingrese un número: ");
			pos = hash->hash(i, len);

			if (arr[pos] == i) {
				cout << "El número está en la posición " << pos << "\n";
			} else {
				// si no se encontró, resolver su posición como si se tratara de una colisión
				if (method == "L")
					hash->searchLineal(arr, len, i, pos);
				else if (method == "C")
					hash->searchCuadratica(arr, len, i, pos);
				else if (method == "D")
					hash->searchDobleHash(arr, len, i, pos);
				// else if (method == "E")
					// hash->searchEncadenamiento();
			}

			break;

		case 0:
			cout << "Adiós\n";
			exit(0);

		default:
			cout << "Opción desconocida\n";
			break;
		}
	}

	return 0;
}
