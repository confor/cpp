#include <iostream>
#include "Hash.h"

int Hash::hash(int clave, int largo) {
	clave = (clave % (largo - 1)) + 1;
	return clave;
}

void Hash::ingresoLineal(int *arr, int largo, int clave, int pos) {
	if ((arr[pos] != '\0') && (arr[pos] == clave)) {
		cout << "Número ya está en la posicion " << pos << "\n";
		return;
	}

	int cont = 0;
	int nuevaPosicion = pos + 1;

	while ((nuevaPosicion != pos) && (arr[nuevaPosicion] != '\0') && (nuevaPosicion <= largo) && (arr[nuevaPosicion] != clave)) {
		nuevaPosicion = nuevaPosicion + 1;
		if (nuevaPosicion == (largo + 1))
			nuevaPosicion = 0;

		cont++;
	}

	// quizá esto puede fallar cuando el arreglo está lleno desde pos hacia la derecha?
	if (arr[nuevaPosicion] == '\0')	{
		arr[nuevaPosicion] = clave;
		cout << "Número insertado en la posición " << nuevaPosicion << "\n";
	}

	if (cont == largo)
		cout << "Error: arreglo lleno" << endl;
}

void Hash::ingresoCuadratica(int *arr, int largo, int clave, int pos) {
	if ((arr[pos] != '\0') && (arr[pos] == clave)) {
		cout << "Número ya está en la posicion " << pos << "\n";
		return;
	}

	int i = 0;
	int nuevaPosicion = pos + (i*i);

	while ((arr[nuevaPosicion] != '\0') && (arr[nuevaPosicion] != clave)) {
		i++;
		nuevaPosicion = pos + (i*i);

		if (nuevaPosicion > largo) {
			i = 0;
			nuevaPosicion = 0;
			pos = 0;
		}
	}

	if (arr[nuevaPosicion] == '\0') {
		arr[nuevaPosicion] = clave;
		cout << "Número insertado en la posición " << nuevaPosicion << "\n";
	} else {
		cout << "Error: número no se pudo insertar\n";
	}
}

void Hash::ingresoDobleHash(int *arr, int largo, int clave, int pos) {
	if ((arr[pos] != '\0') && (arr[pos] == clave)) {
		cout << "Número ya está en la posicion " << pos << "\n";
		return;
	}

	int nuevaPosicion = ((pos + 1) % largo - 1) + 1;

	while ((nuevaPosicion <= largo) && (arr[nuevaPosicion] != '\0') && (nuevaPosicion != pos) && (arr[nuevaPosicion] != clave))
		nuevaPosicion = ((nuevaPosicion + 1) % largo - 1) + 1;

	if (arr[nuevaPosicion] == '\0' || (arr[nuevaPosicion] != clave) ) {
		arr[nuevaPosicion] = clave;

		if (nuevaPosicion == largo + 1)
			cout << "Error: arreglo lleno\n";
		else
			cout << "Número insertado en la posición " << nuevaPosicion << "\n";
	} else {
		cout << "Error: número no se pudo insertar\n";
	}
}

void Hash::searchLineal(int *arr, int largo, int clave, int pos) {
	if ((arr[pos] != '\0') && (arr[pos] == clave) ) {
		cout << "Número ya está en la posicion " << pos << "\n";
		return;
	}

	int nuevaPosicion = pos + 1;

	while ((arr[nuevaPosicion] != '\0') && (nuevaPosicion <= largo) && (nuevaPosicion != pos) && (arr[nuevaPosicion] != clave)){
		nuevaPosicion = nuevaPosicion + 1;
		if (nuevaPosicion == largo + 1)
			nuevaPosicion = 0;
	}

	if ((arr[nuevaPosicion] == '\0') or (nuevaPosicion == pos))
		cout << "No se encontró\n";
	else
		cout << "El número está en la posición " << nuevaPosicion << "\n";
}

void Hash::searchCuadratica(int *arr, int largo, int clave, int pos) {
	if ((arr[pos] != '\0') && (arr[pos] == clave)) {
		cout << "Número ya está en la posicion " << pos << "\n";
		return;
	}

	int i = 1;
	int nuevaPosicion = pos + (i*i);

	while ((arr[nuevaPosicion] != '\0') && (arr[nuevaPosicion] != clave)) {
		i = i + 1;
		nuevaPosicion = pos + (i*i);

		if (nuevaPosicion > largo) {
			i = 1;
			nuevaPosicion = 0;
			pos = 0;
		}
	}

	if (arr[nuevaPosicion] != '\0')
		cout << "No se encontró\n";
	else
		cout << "El número está en la posición " << nuevaPosicion << "\n";
}

void Hash::searchDobleHash(int *arr, int largo, int clave, int pos) {
	if ((arr[pos] != '\0') && (arr[pos] == clave)) {
		cout << "Número ya está en la posicion " << pos << "\n";
		return;
	}

	int nuevaPosicion = ((pos + 1) % largo - 1) + 1;

	while ((nuevaPosicion <= largo) && (arr[nuevaPosicion] != '\0') && (nuevaPosicion != pos) && (arr[nuevaPosicion] != clave))
		nuevaPosicion = ((nuevaPosicion + 1)%largo -1) + 1;

	if ((arr[nuevaPosicion] == '\0') || (nuevaPosicion == pos))
		cout << "No se encontró\n";
	else
		cout << "El número está en la posición " << nuevaPosicion << "\n";
}
